pikepdf (1.0.5+dfsg-3) unstable; urgency=medium

  * Cherry pick upstream commit 4d22fe4 as
    Fix-issue-25-year-missing-leading-zero-on-some-platforms.patch
    (Closes: #928042).

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 28 Apr 2019 18:23:41 -0700

pikepdf (1.0.5+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Fix handling of XMP metadata with no <x:xmpmeta> wrapper.
    - Cherry-pick upstream fix as fix_xmp_metadata_without_xmpmeta_wrapper.patch

 -- Felix Geyer <fgeyer@debian.org>  Wed, 27 Feb 2019 23:33:07 +0100

pikepdf (1.0.5+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 26 Jan 2019 12:54:11 -0700

pikepdf (1.0.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - Add Files-Excluded to d/copyright.
      See https://github.com/pikepdf/pikepdf/issues/21
    - Add disable-test_docinfo_problems.patch
  * Install examples/find_links.py

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 10 Jan 2019 08:44:33 -0700

pikepdf (0.10.1-2) unstable; urgency=medium

  * Upload to unstable.
    Upstream considers the API to be stable.

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 01 Jan 2019 09:19:08 +0000

pikepdf (0.10.1-1) experimental; urgency=medium

  * New upstream release.
    - Add python3-defusedxml, python3-lxml build-deps
    - Add python3-attr autopkgtest dep

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 31 Dec 2018 23:25:17 +0000

pikepdf (0.3.7-1) experimental; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sun, 11 Nov 2018 14:29:02 -0700

pikepdf (0.3.5-1) experimental; urgency=medium

  * New upstream release.

 -- Sean Whitton <spwhitton@spwhitton.name>  Sat, 20 Oct 2018 13:06:33 -0700

pikepdf (0.3.4-1) experimental; urgency=medium

  * New upstream release.
  * Add build-dep on python3-setuptools-scm-git-archive.
  * Update d/copyright for new files.
  * Drop 0001-Restore-Exhibit-B-text-clarify-license-comments-in-r.patch
    Included in this upstream release.
  * Add drop-setuptools_scm_git_archive-from-setup.py.patch.
  * Refresh drop-installation-from-docs-contents.patch.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 10 Oct 2018 08:28:28 -0700

pikepdf (0.3.0-1) experimental; urgency=medium

  * Initial upload, to experimental (Closes: #903625).
    API not yet finalised.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 30 Aug 2018 16:21:39 -0700
